/*
 Написать программу, которая запрашивает у пользователя строку, состоящую
из нескольких слов и целое число n, а затем выводит n - ое слово строки на
экран. В случае неккоректного n выводится сообщение об ошибке
 */

#include <stdio.h>
#include <ctype.h>

#define N 256
#define TRUE 1
#define FALSE 0

int main()
{
    unsigned int    i = 0,
                    j = 0,
                    n = 0,
                    nword = 0,
                    inword = FALSE;
    
    char            str[N] = {0},
                    word[N] = {0};
    
    printf("Input your text: ");
    fgets(str, N, stdin);
    
    printf("Input number of word: ");
    scanf("%d", &n);
    
    for(i = 0; str[i] != '\0'; i++)
    {
        if(!isspace(str[i]) && !inword)
        {
            inword = TRUE;
            nword++;
            j = 0;
        }
        if(isspace(str[i]) && inword)
        {
            inword = FALSE;
            if(nword == n)
            {
                printf("%s\n", word);
                return 0;
            }
        }
        if(!isspace(str[i]) && inword)
        {
            word[j++] = str[i];
        }
    }

    printf("Too many number of word!!!\n");
    return 0;
}