/*
Написать программу, которая находит сумму чисел во введённой строке
Замечание:
Программа рассматривает непрерывные последовательности цифр в строке как
числа и обрабатывает их как единое целое. В программе предусмотреть ограни-
чение на максимальное число разрядов, то есть если пользователь вводит очень
длинную последовательность цифр, её нужно разбить на несколько групп.
*/

#include <stdio.h>

#define N 256
#define LIMIT 3
#define TRUE 1
#define FALSE 0

int power(int a, unsigned int e)
{
    return e == 0 ? 1 : a * power(a, e - 1);
}



int main()
{
    char    str[N] = {0},
            number[N] = {0};
    
    int     i = 0,
            j = 0,
            k = 0,
            l = 0,
            p = 1,
            lennumber = 0,
            sum = 0,
            innumber = FALSE;
    
    printf("Input your line:\n");
    
    fgets(str, N, stdin);
    
    for(i = 0; str[i] != '\0'; i++)
    {
        
        if(!isdigit(str[i]) && innumber) //exit from number
        {
            innumber = FALSE;

            l = 1;
            k = 0;

            while(l <= lennumber / LIMIT)
            {
                p = LIMIT - 1;
                while(k < l * LIMIT)
                    sum = sum + (number[k++] - '0') * power(10, p--);
                //k += (LIMIT);
                l++;
            }
            //tail of numbers
            p = lennumber % LIMIT - 1;
            while(k < lennumber)
                sum = sum + (number[k++] - '0') * power(10, p--);

            j = 0;
            lennumber = 0;
        }
        
        if(isdigit(str[i]) && !innumber) //enter in number
            innumber = TRUE;
        
        if(isdigit(str[i]) && innumber) // we are in number
        {
            number[j++] = str[i];
            lennumber++;
        }
    }
    
    printf("\nSum = %d!\n", sum);
    return 0;
}