/*
 Написать программу, которая переводит рост из американской си-
стемы (футы, дюймы) в европейскую (сантиметры). Данные вво-
дятся в виде двух целых чисел, выводятся в виде вещественного
числа с точностью до 1 знака.
 */

#include <stdio.h>



int main()
{
    unsigned    ft = 0,
                in = 0;

    char        angletype   = 0,
                bufclear    = 0, 
                restart     = 0;

    while(1)
    {
        printf("Input your lenght in ft.in => ");

        if(scanf("%u.%u", &ft, &in) != 2)
        {
            printf("Wrong input\n");
            while(bufclear != '\n')
                scanf("%c", &bufclear);//clear buffer
            bufclear = 0;
            continue;
        }

        //check symbols after input
        scanf("%c", &bufclear);
        if(bufclear != '\n')
        {
            printf("Wrong input\n");
            while(bufclear != '\n')
                scanf("%c", &bufclear);//clear buffer
            bufclear = 0;
            continue;
        }

        while(bufclear != '\n')
            scanf("%c", &bufclear);//clear buffer
        bufclear = 0;

        printf("Your American lenght %dft %din is equal %.1f Russian cm\n", 
                ft, in, (float)ft * 12.0 * 2.54 + (float)in * 2.54);

        while(1)
        {
            printf("Restart? (y/n): ");

            if((scanf("%c", &restart) != 1) || ((restart != 'y') && (restart != 'n')))
            {
                printf("Wrong input\n");
                if(restart != '\n')
                    while(bufclear != '\n')
                        scanf("%c", &bufclear);//clear buffer
                bufclear = 0;
                continue;
            }

            //check symbols after char
            scanf("%c", &bufclear);
            if(bufclear != '\n')
            {
                printf("Wrong input\n");
                while(bufclear != '\n')
                    scanf("%c", &bufclear);//clear buffer
                bufclear = 0;
                continue;
            }

            if(restart == 'y')
                break;
            else
                return 0;
            }
    }
}