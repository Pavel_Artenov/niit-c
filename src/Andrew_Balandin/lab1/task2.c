/*
Написать программу, которая запрашивает текущее время в фор-
мате ЧЧ:ММ:СС, а затем выводит приветствие в зависимости от
указанного времени ("Доброе утро "Добрый день"и т.д.)
*/


#include <stdio.h>

int main()
{
    unsigned int    hh = 0, 
                    mm = 0,
                    ss = 0;

    char            bufclear = 0, 
                    restart = 0;
    
    while(1)
    {
        printf("Input TIME in format HH:MM:SS => ");

        if((scanf("%u:%u:%u", &hh, &mm, &ss) != 3) || (hh > 23) || (mm > 59) || (ss > 59))
        {
            printf("Wrong input\n");
            while(bufclear != '\n')
                    scanf("%c", &bufclear);//clear buffer
            bufclear = 0;
            continue;
        }

        //check symbols after input
        scanf("%c", &bufclear);
        if(bufclear != '\n')
        {
            printf("Wrong input\n");
            while(bufclear != '\n')
                scanf("%c", &bufclear);//clear buffer
            bufclear = 0;
            continue;
        }

        while(bufclear != '\n')
            scanf("%c", &bufclear);//clear buffer
        bufclear = 0;

        if((hh >= 0 && hh <= 4) || (hh >= 21 && hh <= 23))
            printf("Good night!\n");
        else if((hh >= 5 && hh <= 10))
            printf("Good mourning!\n");
        else if((hh >=11 && hh <= 15))
            printf("Good day!\n");
        else
            printf("Good evening!\n");


        while(1)
        {
            printf("Restart? (y/n): ");

            if((scanf("%c", &restart) != 1) || ((restart != 'y') && (restart != 'n')))
            {
                printf("Wrong input\n");
                if(restart != '\n')
                    while(bufclear != '\n')
                        scanf("%c", &bufclear);//clear buffer
                bufclear = 0;
                continue;
            }

        //check symbols after char
        scanf("%c", &bufclear);
        if(bufclear != '\n')
        {
            printf("Wrong input\n");
            while(bufclear != '\n')
                    scanf("%c", &bufclear);//clear buffer
            bufclear = 0;
            continue;
        }
        while(bufclear != '\n')
            scanf("%c", &bufclear);//clear buffer
        bufclear = 0;

        if(restart == 'y')
            break;
        else
            return 0;
        }
    }
}