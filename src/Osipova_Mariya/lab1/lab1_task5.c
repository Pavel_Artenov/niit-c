#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>

int main()
{
	int dlina=0;
	int probel=0;
	char str[80] = { 0 };

	printf("Vvedite stroku:\n");
	fgets(str, 80,stdin);
	str[strlen(str) - 1] = 0;
	dlina = strlen(str);
	probel = (80 - dlina) / 2;

	while (probel > 0)
	{
		printf(" ");
		probel--;
	}

	printf("%s\n", str);

	return 0;
}