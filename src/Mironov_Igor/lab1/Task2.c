//������� �2 - ����������� �� ������� �����.

#include <stdio.h>
#include <conio.h>

int main() {

	unsigned int h, m, s;
	int c;

	//���� ���� �������� � ���������

	while (1) {
		printf("What time is it now - hh:mm:ss?\n");
		if ((scanf_s("%u:%u:%u", &h, &m, &s) != 3) || h > 23 || m > 60 || s > 60) {
			printf("Input error!\n");
			do {
				c = getchar();
			} while (c != '\n' && c != EOF);
			continue;
		}
		else
			break;
	}

	// ����������� �� ������� �����
	
	if ((h > 3 && h < 12))
		printf("Good day!\n");

	if ((h > 11 && h < 17))
		printf("Dinner!\n");

	if ((h > 16 && h < 23))
		printf("Good afternoon!\n");

	if ((h > 22 && h < 24) || (h >= 0 && h < 4))
		printf("Good night!\n");

	return 0;
 }