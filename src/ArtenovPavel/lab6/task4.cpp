/*��������� ��������� ������ ������������ � ����������� ���������.*/
#include "stdio.h"
#include "math.h"
#include "time.h"
#include "stdlib.h"
long int sumq=0;

void fillArr(int * ptd,float N)
{
	for(int i=0;i<N;i++)
		ptd[i]=rand () % 100;                  //��������� ������ ���������� �� 0 �� 99
	
}
void sumByIter(int * ptd,float N)
{
	int sum=0;
	for(int i=0;i<N;i++){
		sum+=ptd[i];
	}
	printf("sum=%d\n",sum);
}
int sumByRec(int * ptd,float N,int i)
{
	long int temp=0;

	if(i==N-1){
		//printf("sum=%d i=%d\n",sumq,i);
		return ptd[i];
	}
	
	else{
		i++;
		temp=sumByRec(ptd,N,i);
		sumq=sumq+temp;
	}
	
	printf("sum=%d i=%d\n",sumq,i);
	return ptd[i-1];
	
}

int main()
{
	int * ptd;
	int start,finish;
	float M,N;
	start=clock();
	srand(time(NULL));
	printf("Enter M,10 to 20\n");
	scanf("%f",&M);
	N = pow((float)2.0,M);
	//printf("N=%f\n",N);
	ptd = (int *) malloc (N * sizeof(int));      //�������� ���� ������ �� N �������� int
	fillArr(ptd,N);
	start=clock();
	sumByIter(ptd,N);
	finish=clock() - start;
	printf("run time iter = %f ms\n",finish);
	start=clock();
	sumq+=sumByRec(ptd,N,0);
	printf("sum=%d\n",sumq);
	finish=clock() - start;
	printf("run time rec = %f ms\n",finish);
	finish=clock() - start;
	printf("%d\n",finish);
	free(ptd);
	
	return 0;

}