/*
��������� ��������� �� ������������ ������ � ������� �� �� �����,
��������� ����� � ��������� �������.
��������� ������� �� ���� �������:
1)printWord - ������� ����� �� ������.
2)getWords - ��������� ������ ���������� �������� ������ ���� ����.
3)main - �������� �������.
*/

#include "stdio.h"
#include "string.h"
#include "time.h"
#include "stdlib.h"
#include "ctype.h"

#define size_arr 256

int getWords(char * str,char **start_word);
void printWord(char **start_word,int words);

int main()
{
	int w=0;
	char str[size_arr];
	char * start_word[size_arr/2];

	fgets(str,size_arr,stdin);
	str[strlen(str)-1] = 0;
	srand(time(NULL));

	w = getWords(str,start_word);
	printWord(start_word,w);

	return 0;
}
int getWords(char * str,char **start_word)    //��������� � ������ ���������� �� ������ �����
{
	int inword = 0,i = 0,words = 0,j = 0;
	while(str[i] != 0){
		if (str[i] == ' ')
			inword=0;
		
		else if (inword == 0){
			words++;
			inword = 1;
			start_word[j] = str + i;
			j++;
		}
		i++;
	}
	return words;
}
void printWord(char **start_word,int words)   //����� ���� � ��������� �������
{
	srand(time(NULL));
	char * p;
	int count;
	char ch;
	while(words>0){
		count = rand() % words;
		p = start_word[count];
		
		do{
			ch = *p;
			printf("%c",ch);
			p++;
		}
		while (ch != ' ' && ch != 0);
		start_word[count] = start_word[words-1];
		words--;
	}
}
